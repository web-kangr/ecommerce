// pages/search_page/search_page.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        prodName:"", // 搜索默认值
        recentSearch:[], // 搜索历史
    },

    /**
     * 生命周期函数--监听页面加载  第一次显示页面的时候调用
     */
    onLoad: function () {
        this.searchHistory()
    },
    // 返回首页
    canceles:function(){
        wx.navigateBack({
            // url: '/pages/index/index',
        })
    },
    // 输入内容 绑定输入值
    searchContent:function(e){
        this.setData({
            prodName:e.detail.value,
        })
    },
    // 获取历史搜索
    searchHistory:function(){
        const searchData = wx.getStorageSync('recentSearch')
        this.setData({
            recentSearch:searchData,
        })

    },
    // 搜索结果
    searchResult:function(){
        // 去掉多余的空格
        if(this.data.prodName.trim()){
            let recentSearch = wx.getStorageSync('recentSearch') || []  
            recentSearch = recentSearch.filter(item => item !== this.data.prodName)
            recentSearch.unshift(this.data.prodName);  // 添加到数组的开头
            // console.log(recentSearch);
            if(recentSearch.length>5){
                recentSearch.pop()  // 从数组中删除最后一个元素
            }
            wx.setStorageSync('recentSearch',recentSearch)
            this.setData({
                recentSearch:recentSearch,
                // prodName:""
            });
            wx.navigateTo({
                url:"/pages/search_result/search_result?prodName="+this.data.prodName
            })
        }
    },
    // 删除 clearStorage = 清除所有  removeStorage=单个缓存
    delete:function(){
        wx.clearStorage('recentSearch')
        this.searchHistory()
    },
    // 颠倒
    rever:function(e){
        console.log(e.currentTarget.dataset.name);
        this.setData({
            prodName:e.currentTarget.dataset.name
        })
    },




    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})