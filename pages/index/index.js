// index.js
const electric = require('../../server/index')
const HTTP = new electric()

Page({
  data: {
    indicatorDots: true,
    autoplay: true,   // 是否自动切换
    indexImgs:[],     // 横向轮播
    topNoticeList:[], // 纵向轮播
    uppernew:[],      // 每日上新
    commodity:[],     // 商品
  },
  onLoad:function(){
    this.transverse()   // 横向轮播
    this.longitudinal() // 纵向轮播
    this.uppernew()     // 每日上新
    this.prodListBy()   // 生产
  },
  // 轮播图
  transverse(){
    HTTP.name().then(res=>{
      this.setData({
        indexImgs:res
      })
    })
  },
  longitudinal(){ // 纵
    HTTP.topNot().then(res=>{
      // console.log(res);
      this.setData({
        topNoticeList:res
      })
    })
  },
  uppernew(){  // 每日上新 等
    HTTP.prodTagList().then(res=>{
      console.log(res,'上新');
      this.setData({
        uppernew:res
      })
    })
  },
  prodListBy(){  // 商品
    HTTP.prodListByList({tagId:3,size:6}).then(res=>{
      // console.log(res.records,'12345');
      this.setData({
        commodity:res.records
      })
    })
  },
  // 指示点
  changeIndicatorDots() {
    this.setData({
      indicatorDots: !this.data.indicatorDots
    })
  },
  // 新品推荐
  newProducts:function(){
    wx.navigateTo({
      url: '/pages/new_products/new_products',
    })
  },
  timeLimit:function(){
    wx.navigateTo({
      url: '/pages/time_limit/time_limit',
    })
  },
  crazyRobbery:function(){
    wx.navigateTo({
      url: '/pages/crazy_robbery/crazy_robbery',
    })
  },
  // 搜索
  searchPage:function(){
    wx.navigateTo({
      url: '/pages/search_page/search_page',
    })
    console.log(99)
  },
  // 最新公告
  noticePage:function(){
    wx.navigateTo({
      url:'/pages/notice_page/notice_page'
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh()   // 停止当前页面下拉刷新
    // setTimeout(function(){
    //   wx.hideNavigationBarLoading()  // 完成停止加载
    //   wx.stopPullDownRefresh() // 停止下拉加载
    // },2000)
  },
})