// pages/shopping/shopping.js
const shopping_http=require("../../server/shopping")
const newShoppingHttp=new shopping_http()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    shopping_list:[],
    showNewData:[],
    allCheckedShopResult:{},
    allChecked:false,
    checkedIds:[]
  }, 
getInfoData(){
   //获取用户商品的信息
  newShoppingHttp.shopping_Api().then((res)=>{
    console.log(res,"数据"); 
    let newData=[]
    res.forEach(item=>{
      if(item.shopCartItemDiscounts &&item.shopCartItemDiscounts.length){
        item.shopCartItemDiscounts.forEach(val=>{
          newData=  [...newData,...val.shopCartItems ]
        })
      }
    })
    console.log(newData);
    newData=newData.map(item=>({...item,checked:false}))
    this.setData({ 
      shopping_list:res,
      showNewData:newData,
      checkedIds:[],
      allChecked:false 
    })
  })
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // newShoppingHttp.shopping_Api().then((res)=>{
    //   console.log(res,"数据"); 
    //   let newData=[]
    //   res.forEach(item=>{
    //     if(item.shopCartItemDiscounts &&item.shopCartItemDiscounts.length){
    //       item.shopCartItemDiscounts.forEach(val=>{
    //         newData=  [...newData,...val.shopCartItems ]
    //       })
    //     }
    //   })
    //   console.log(newData);
    //   newData=newData.map(item=>({...item,checked:false}))
    //   this.setData({ 
    //     shopping_list:res,
    //     showNewData:newData
    //   })
    // })
   this.getInfoData()
  },
  //点击单选按钮选中商品
  checkboxChange(e){
    console.log(e,"eeeee");
    let checkedIds=e.detail.value
    // console.log(checkedIds);
    this.setData({
      allChecked:checkedIds.length===this.data.showNewData.length,
      checkedIds
    })
    this.getAllPays(checkedIds)
   },
  //获取选中购物项总计、选中的商品数量
  getAllPays(checkedIds){
    newShoppingHttp.getTotalPay(checkedIds).then((res)=>{
      // this.checkboxChange()
      console.log(res,"res");
      this.setData({
        allCheckedShopResult:res
      })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  change(e){
  this.data.showNewData.filter((item,index)=>{
     if(item.basketId==e.target.dataset.ids){
      return item.checked=!item.checked
     }
    })
  },
  //点击全选选中所有商品对商品进行总价和数量的功能实现
  changeAllChecked(){
    let allChecked=this.data.allChecked
    let showNewData=this.data.showNewData
    showNewData=showNewData.map(item=>({...item,checked:!allChecked}))
    console.log(showNewData);
    let ids=showNewData.map(item=>item.basketId)
    this.getAllPays(allChecked?[]:ids)
    this.setData({
      allChecked:!allChecked,
      showNewData,
      checkedIds:allChecked ? []:ids
    })
  },
  
  //选中商品点击添加删除商品数量和总价功能的实现
  changeItem(e){
    console.log(e,"eeeeee");
    let shopMsg=e.currentTarget.dataset   
    console.log(shopMsg);
    
    let showNewData=this.data.showNewData
    console.log(showNewData);
    showNewData[shopMsg.index].prodCount=shopMsg.count==="1" ? showNewData[shopMsg.index].prodCount+1:showNewData[shopMsg.index].prodCount-1;
    this.setData({
      showNewData:[...showNewData]
    })
    //添加、修改用户购物车物品 
    newShoppingHttp.changeItem({
      count:shopMsg.count,
      shopId:shopMsg.shop,
      skuId:shopMsg.sku,
      prodId:shopMsg.prod, 
    }).then((res)=>{ 
      console.log(res,"res");
      this.getAllPays(this.data.checkedIds)
    })

  },
  ////删除用户购物车物品
  deleteShop(){
    newShoppingHttp.deleteShop(this.data.checkedIds).then((res)=>{
      console.log(res);
      this.getInfoData()
    })
  },  
  to_account(){
      if(this.data.checkedIds.length>0){
        // console.log("用户选择商品了");
        wx.setStorageSync('basketIds', this.data.checkedIds)
        wx.navigateTo({
          url: '/pages/order/order',
        })
      }else{
        console.log("请选择商品");
         wx.showToast({
           title: '请选择商品',
           icon:"none"
         })
      }
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})




