// pages/crazy_robbery/crazy_robbery.js
const electric = require('../../server/index')
const HTTP = new electric()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        commodity:[],     // 商品
    },
    
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function () {
        this.prodListBy()   // 生产
    },
    prodListBy(){  // 商品
      console.log(this.data);
      HTTP.prodListByList({tagId:3,size:10}).then(res=>{
        console.log(res.records,'12345');
        this.setData({
          commodity:res.records
        })
      })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})