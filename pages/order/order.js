// pages/order/order.js
const shopping_http=require("../../server/shopping")
const newShoppingHttp=new shopping_http()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    shopping_list:[],
    showNewData:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let addrId=wx.getStorageSync('address') || 0; 
    let basketIds=wx.getStorageSync('baskIds');
    newShoppingHttp.getOrderDetail({
      addrId:addrId,
      basketIds,
      couponIds: [],
      userChangeCoupon: 1
    }).then((res)=>{
      let newData=[]
      console.log(res);
      res.shopCartOrders.forEach(item=>{
        if(item.shopCartItemDiscounts &&item.shopCartItemDiscounts.length){
          item.shopCartItemDiscounts.forEach(val=>{
            newData=  [...newData,...val.shopCartItems ]
          })
        }
      })
      console.log(newData);
      
      this.setData({ 
        shopping_list:res,
        showNewData:newData,
       
      })
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})