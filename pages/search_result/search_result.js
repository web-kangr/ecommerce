// pages/search_result/search_result.js
const electric = require('../../server/index')
const HTTP = new electric()

Page({
    /**
     * 页面的初始数据
     */
    data: {
        sts:0,
        prodName:"",
        showType:true,
        resultList:[],
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        console.log(options);
        this.setData({
            prodName:options.prodName
        })
        this.prodListBy()
    },
    // 数据
    prodListBy(){
        console.log(this.data,'1234');
        HTTP.searchProdPageList({prodName:this.data.prodName,sort:this.data.sts}).then(res=>{
            console.log(res.records);
            this.setData({
                resultList:res.records
            })
        })
      },
      // 搜索
    searchContent:function(e){
        console.log(e.detail.value);
        this.setData({
            prodName:e.detail.value
        })
    },
    // 二次搜索
    searchResult:function(){
        this.prodListBy()
    },
    // img切换
    canceles:function(){
        var showType = this.data.showType
        if(showType==false){
            showType=true
        }else{
            showType=false
        }
        this.setData({
            showType:showType
        })
    },
    // tab切换
    onStsTap: function(e) {
        // console.log(e);
        this.setData({
            sts: e.currentTarget.dataset.sts,
        });
    },


    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})