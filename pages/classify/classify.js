// pages/classify/classify.js
const HTTP = require("../../server/classify");
const NEWHTTP = new HTTP();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    tagData: [], //分类左数据
    optTag: 0, //选中的下标
    listData: [], //数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    // 获取选择标签
    NEWHTTP.tagsApi().then((res) => {
      this.setData({
        tagData: res,
      });
      // 获取列表数据
      this.getListData(0);
    });
  },
  // 获取列表数据
  getListData(ind) {
    NEWHTTP.listData({ id: this.data.tagData[ind].categoryId }).then((res) => {
      console.log(res, "listData");
      this.setData({
        listData: res,
      });
    });
  },
  // 跳转详情页
  optContentItem: function (e) {
    wx.navigateTo({
      url: `/pages/detail/detail?prodId=${e.currentTarget.dataset.prodid}`,
    });
  },
  // 跳转到搜索页面
  searchPage(){
    wx.navigateTo({
      url: '/pages/search_page/search_page',
    })
  },
  // 点击的每一项
  optTag: function (e) {
    this.setData({
      optTag: e.target.dataset.id,
    });
    this.getListData(e.target.dataset.id);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    console.log(23524234);
  },
});
