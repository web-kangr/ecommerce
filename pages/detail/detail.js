// pages/detail/detail.js
const HTTP = require("../../server/detail");
const NEWHTTP = new HTTP();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    detailData: [], //详情数据
    imgs: "", //图片
    prod: [], //评论的个数
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    interval: 2000,
    duration: 500,
    banner: "", //广告介绍图
    shpFlag: false, //购物车弹框的显示
    ColleFlag: "", //收藏的状态
    ShoppingFlag: false, //购物车弹窗的显示
    Selshop: [], //商品选择
    affSelshop: [], //确认后的商品详情，默认为第一条数据
    shopNum: 1, //选择的商品数量
    specificationTitle: [], //规格标题
    specification: [], //选择商品的规格
    evaluteFlag: false, //评价弹窗的显示控制
    evaltagsNumInd: 0, //评价弹框tab切换
    newSkuData: [], //处理后的sku数据
    tapShop: {}, //用户选择的商品规格
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const { prodId } = options;
    this.prodComm(prodId);
    this.Colle(prodId);
    NEWHTTP.detailData({ id: prodId }).then((res) => {
      // console.log(res.skuList[0]);
      let imgStr = res.imgs;
      let imgs = imgStr.split(",");
      this.setData({
        detailData: res,
        imgs,
        banner: res.content,
        Selshop: res.skuList,
        affSelshop: res.skuList[0],
      });
      // console.log(this.data.affSelshop, "affSelshop");
      this.ShopData(res.skuList);
      // console.log(this.data.tapShop, "tapShop");
    });
  },
  // 加入购物车
  ShopTapToc() {
    console.log(this.data.detailData, "---detailData----");
    NEWHTTP.AddShopping({
      basketId: 0,
      count: this.data.shopNum,
      prodId: this.data.detailData.prodId,
      shopId: this.data.detailData.shopId,
      skuId: 385,
    }).then((res) => {
      console.log(res);
    });
  },
  // 商品的数量减
  lowerShopNum() {
    console.log(this.data.shopNum--);
    if (this.data.shopNum < 1) {
      console.log(this.data.shopNum--);
      this.setData({
        shopNum: this.data.shopNum--,
      });
    }
  },
  // 商品数量的加
  addShopNum() {
    console.log(this.data.shopNum++);
    this.setData({
      shopNum: this.data.shopNum++,
    });
  },
  // 用户选择商品的规格
  tapShopType(e) {
    // console.log(e.target.dataset);
    this.setData({
      tapShop: {
        ...this.data.tapShop,
        [e.target.dataset.title]: e.target.dataset.name,
      },
    });
  },
  getShoppingPage() {
    wx.reLaunch({
      url: "/pages/shopping/shopping",
    });
  },
  // 评价弹框的显示
  onEvalute() {
    this.setData({
      evaluteFlag: true,
    });
  },
  // 评价弹框的隐藏
  offEvaluate() {
    this.setData({
      evaluteFlag: false,
    });
  },
  // 评价弹框的tab切换
  evaltagsNum(e) {
    this.setData({
      evaltagsNumInd: e.currentTarget.dataset.evaltagesindex,
    });
  },
  // 商品规格的数据处理
  ShopData: function (ties) {
    let obj = {};
    ties.forEach((item) => {
      let types = item.properties.split(";");
      types.forEach((val) => {
        let [key, value] = val.split(":");
        obj[key] ? obj[key].push(value) : (obj[key] = [value]);
      });
    });
    let res = Object.keys(obj).map((ite) => {
      return {
        title: ite,
        list: [...new Set([...obj[ite]])],
      };
    });
    this.setData({
      newSkuData: res,
    });
  },
  // 收藏   修改完状态重新获取
  tapColle: function () {
    let id = this.data.detailData.prodId;
    NEWHTTP.UpCollection({ id: this.data.detailData.prodId }).then((res) => {
      this.Colle(id);
    });
  },
  //   评论的数据获取
  prodComm: function (id) {
    NEWHTTP.prodComm({ id }).then((res) => {
      // console.log(res, "prodComn");
      this.setData({
        prod: res,
      });
    });
  },
  // 是否收藏
  Colle: function (id) {
    // console.log(id, "colle");
    NEWHTTP.isCollection(id).then((res) => {
      this.setData({
        ColleFlag: res,
      });
    });
  },
  //购物车弹窗的显示
  ShopTap: function () {
    this.setData({
      ShoppingFlag: true,
    });
  },
  // 关闭购物车的弹框
  closeShop: function () {
    this.setData({
      ShoppingFlag: false,
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {},
});
