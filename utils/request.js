const { baseURL } = require("../config/index");
class Request {
  request(api, method, data) {
    if (api === "/login?grant_type=mini_app") {
      return new Promise((resolve, reject) => {
        this._request(resolve, reject, api, method, data);
      });
    } else {
      let token = wx.getStorageSync("token");
      if (token) {
        return new Promise((resolve, reject) => {
          this._request(resolve, reject, api, method, data);
        });
      } else {
        wx.showToast({
          title: "请登录！",
          icon: "error",
        });
        return;
      }
    }
  }
  _request(resolve, reject, api, method = "GET", data = {}) {
    wx.request({
      url: `${
        api.includes("/broadcast/room")
          ? "https://sign.jasonandjay.com"
          : baseURL
      }${api}`,
      timeout: 5000,
      method,
      data,
      header: {
        Authorization: wx.getStorageSync("token")
          ? "bearer" + wx.getStorageSync("token")
          : undefined,
      },
      success(res) {
        wx.showToast({
          title: "获取成功！",
          icon: "success",
          duration: 2000,
        });
        resolve(res.data);
      },
      fail() {
        wx.showToast({
          title: "获取失败！",
          icon: "error",
          duration: 2000,
        });
        reject("获取失败！");
      },
    });
  }
}

module.exports = Request;
