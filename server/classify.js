const Request = require("../utils/request");
const { classifgURL, baseURL } = require("../config/index");
class API extends Request {
  tagsApi() {
    return this.request(`/category/categoryInfo`, "GET", "parentId");
  }
  listData({id}){
    return this.request(`/prod/pageProd?categoryId=${id}`,"GET")
  }
}

module.exports = API;
