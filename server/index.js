const Request = require('../utils/request')

class API extends Request {
  name() {//上
    return this.request('/indexImgs', "GET", )
  }
  topNot() {
    return this.request('/shop/notice/topNoticeList', "GET", )
    // https://bjwz.bwie.com/mall4j/shop/notice/info/2
  }
  noticeList() {// 最新公告
    return this.request('/shop/notice/noticeList', "GET", )
  }
  notice_detail({id}){  // 通知详情
    console.log(id);
    return this.request(`/shop/notice/info/${id}`, "GET" )
  }
  prodTagList() {// 下
    return this.request('/prod/tag/prodTagList', "GET", )
  }
  prodListByList({tagId,size}) { // 6条商品
    return this.request(`/prod/prodListByTagId?tagId=${tagId}&size=${size}`, "GET", )
  }
  searchProdPageList({prodName,sort}) { // 搜索
    console.log(prodName,sort);
    return this.request(`/search/searchProdPage?current=1&prodName=${prodName}&size=10&sort=${sort}`, "GET" )
  }
  
}


module.exports = API
