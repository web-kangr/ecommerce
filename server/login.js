const Request = require("../utils/request");
class Login extends Request {
  loginApi({ code }) {
    return this.request("/login?grant_type=mini_app", "POST", {
      principal: code,
    });
  }
}
module.exports = Login;
