const Request = require('../utils/request')
class API extends Request {
   //获取用户商品的信息
  shopping_Api(){
    return this.request(`/p/shopCart/info`,"POST")
  }
  //获取选中购物项总计、选中的商品数量
  getTotalPay(params){
    console.log(params);
    return this.request(`/p/shopCart/totalPay`,"POST",params)
  }
  //添加、修改用户购物车物品
  changeItem(params){
    return this.request(`/p/shopCart/changeItem`,"POST",params)
  }
  //删除用户购物车物品
  deleteShop(ids){
    return this.request(`/p/shopCart/deleteItem`,"DELETE",ids)
  }
  getOrderDetail(params){
    return this.request(`/p/order/confirm`,"POST",params)
  }
}
module.exports = API