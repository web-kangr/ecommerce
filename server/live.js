const Request = require("../utils/request");

class API extends Request {
  GetLiveList() {
    this.request("/broadcast/room", "GET");
  }
}

module.exports = API;
