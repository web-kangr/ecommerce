const Request = require("../utils/request");
class HTTP extends Request {
  detailData({ id }) {
    return this.request(`/prod/prodInfo?prodId=${id}`);
  }
  prodComm({ id }) {
    return this.request(`/prodComm/prodCommData?prodId=${id}`);
  }
  isCollection(id) {
    return this.request(`/p/user/collection/isCollection?prodId=${id}`);
  }
  UpCollection({ id }) {
    return this.request(`/p/user/collection/addOrCancel`, "POST", id);
  }
  AddShopping(data) {
    return this.request(`/p/shopCart/changeItem`, "post", data);
  }
}
module.exports = HTTP;
