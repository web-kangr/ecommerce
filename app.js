// app.js
const HTTP = require('./server/login')
const NEWHTTP = new HTTP();
App({
  onLaunch() {
    // setStorageSync
    // getStorageSync
    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        NEWHTTP.loginApi({code:res.code}).then((res)=>{
          wx.setStorageSync('token', res.access_token)
        })
      }
    })
  },
  globalData: {
    userInfo: null
  }
})
